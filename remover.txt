# Build Mario

stages:
  - build
  - deploy

services:
  - docker:20.10.16-dind

build:
  stage: build
  image: docker:20.10.16
  variables:
    DOCKER_TLS_CERTDIR: ""
  before_script:
    - VERSION=$(grep '"version"' package.json | cut -d '"' -f 4 | head -n 1)
  script:
    - docker build -t $dockerhub_user/mario:$VERSION .
    - docker login -u $dockerhub_user -p $dockerhub_password
    - docker push $dockerhub_user/mario:$VERSION

deploy:
  stage: deploy
  image: alpine
  before_script:
    - apk add openssh-client
    - eval "$(ssh-agent -s)"
    - mkdir -p ~/.ssh
    - echo "$SSH_PRIVATE_KEY" > ~/.ssh/id_rsa
    - chmod -R 700 ~/.ssh
    - ssh-add ~/.ssh/id_rsa
    - VERSION=$(grep '"version"' package.json | cut -d '"' -f 4 | head -n 1)
  script:
    - ssh -o StrictHostKeyChecking=no ubuntu@$app_server docker rm -f mario
    - ssh -o StrictHostKeyChecking=no ubuntu@$app_server docker run -itd --restart=always --name mario -p 3000:3000 $dockerhub_user/mario:$VERSION  

